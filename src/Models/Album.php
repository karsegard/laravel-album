<?php

namespace KDA\Album\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\InteractsWithMedia;

class Album extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'images',
        'credits'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];

    protected $appends = [
        'backpack_identifier',
    ];

    protected static function newFactory()
    {
        return  \KDA\Album\Database\Factories\AlbumFactory::new();
    }


    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(256)
            ->height(256)
            ->sharpen(10);
        $this->addMediaConversion('md')
            ->width(1024)
            ->height(1024);
        $this->addMediaConversion('sm')
            ->width(512)
            ->height(512);
        $this->addMediaConversion('xl')
            ->width(2048)
            ->height(2048);
    }

    public function getConversionsAttribute(){
        return ['md','sm','xl','thumb'];
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('photos');
    }

    public function getImagesAttribute()
    {
        return $this->getMedia('photos');
    }
    public function getHomeImagesAttribute()
    {
        return $this->getMedia('photos', ['home' => 1]);
    }

    
    public static function getAll()
    {
        $albums = Album::all();
        $results = $albums->reduce(function ($carry, $item) {
            $carry->push($item->getAlbum());
            return $carry;
        }, collect([]));
        return $results;
    }

    public function getAlbum(){
        $images = $this->home_images;
        
            if (!$images || $images->count() == 0) {
                $images =  $this->images;
            }

            $images = $images->reduce(function ($responsives, $image) {
                $bp = ['thumb', 'sm', 'md', 'xl'];
                foreach ($bp as $b) {
                    $im[$b] = $image->getUrl($b);
                }
                $responsives->push($im);
                return $responsives;
            }, collect([]));
        return collect([
            'album'=>$this,
            'annotation' => $this->title,
            'images' => $images
        ]);
    }
}

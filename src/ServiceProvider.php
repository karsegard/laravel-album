<?php

namespace KDA\Album;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasRoutes;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasLoadableMigration;

    protected $packageName='kda-album';
  
    protected $configs = [
        'kda/album.php'=> 'kda.album'
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
}

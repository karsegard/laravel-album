<?php

namespace KDA\Album\Database\Factories;

use KDA\Album\Models\Album;
use Illuminate\Database\Eloquent\Factories\Factory;

class AlbumFactory extends Factory
{
    protected $model = Album::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
